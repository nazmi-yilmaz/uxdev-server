const AuthService = require('../services/AuthService')
const asyncHandler = require('../middlewares/async')

module.exports.login = asyncHandler(async (req, res, next) => {
  let { email, password } = req.body

  let result = await AuthService.login(email, password)

  res.status(200).json({
    success: true,
    token: result.token,
    id: result.user._id,
    role: result.user.role,
  })
})

module.exports.forgotPassword = asyncHandler(async (req, res, next) => {
  let email = req.body.email

  ///
})
