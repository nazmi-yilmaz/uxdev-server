const ProjectService = require('../services/ProjectService')
const asyncHandler = require('../middlewares/async')

module.exports.create = asyncHandler(async (req, res, next) => {
  let user = req.user

  if (user) {
    let project = await ProjectService.create(req.body, user._id)

    return res.status(201).json({
      success: true,
      project: project,
    })
  }

  let project = await ProjectService.create(req.body)

  res.status(201).json({
    success: true,
    project: project,
  })
})

module.exports.get = asyncHandler(async (req, res, next) => {
  let user = req.user
  let projectId = req.params.id

  let project = await ProjectService.get(projectId)

  if (project.owner.toString() !== user.id && user.role !== 'admin') {
    return res.status(403).json({
      success: false,
      error: 'Permission denied to the source',
    })
  }

  res.status(200).json({
    success: true,
    project: project,
  })
})

module.exports.query = asyncHandler(async (req, res, next) => {
  let projects = await ProjectService.query(req.query)
  res.status(200).json(projects)
})

module.exports.update = asyncHandler(async (req, res, next) => {
  let props = req.body
  let projectId = req.params.id

  let project = await ProjectService.update(projectId, props)

  res.status(200).json({
    success: true,
    project: project,
  })
})

module.exports.del = asyncHandler(async (req, res, next) => {
  let projectId = req.params.id

  let project = await ProjectService.delete(projectId)

  return res.status(200).json({
    success: true,
    project: project,
  })
})

module.exports.confirm = asyncHandler(async (req, res, next) => {
  let projectId = req.params.id

  let project = await ProjectService.confirm(projectId)

  res.status(200).json({
    success: true,
    message: `Project :${project.id} is confirmed`,
  })
})

module.exports.reject = asyncHandler(async (req, res, next) => {
  let projectId = req.params.id
  let message = req.body.message

  let project = await ProjectService.reject(projectId, message)

  res.status(200).json({
    success: true,
    message: `Project : ${project.id} is rejected`,
  })
})

module.exports.stats = asyncHandler(async (req, res, next) => {
  let stats = await ProjectService.stats()

  res.status(200).json({
    success: true,
    stats: stats,
  })
})
