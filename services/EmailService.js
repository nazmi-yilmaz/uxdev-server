const sgmail = require('@sendgrid/mail')
const ErrorResponse = require('../utils/ErrorResponse')

sgmail.setApiKey(process.env.SENDGRID_API_KEY)

module.exports.sendUserInit = async (user, password, project) => {
  await this.send({
    user: user,
    templateId: process.env.USER_INIT_TEMPLATE_ID,
    data: {
      subject: `Welcome to uxdev.co`,
      link: process.env.LOGIN_PAGE,
      project: project,
      password: password,
    },
  })
}

module.exports.sendConfirmed = async (user, project) => {
  await this.send({
    user: user,
    templateId: process.env.PROJECT_CONFIRMED_TEMPLATE_ID,
    data: {
      subject: `Hi, Your Project is Confirmed`,
      link: process.env.LOGIN_PAGE,
      project: project,
    },
  })
}

module.exports.sendRejected = async (name, email, message) => {
  await this.send({
    user: {
      name,
      email,
    },
    templateId: process.env.PROJECT_REJECTED_TEMPLATE_ID,
    data: {
      subject: 'Hi, Unfortunately, Your Project is not Confirmed',
      message: message,
    },
  })
}

module.exports.sendResetPassword = async (user) => {
  await this.send({
    user: user,
    templateId: process.env.FORGOT_PASSWORD_TEMPLATE_ID,
    data: {
      subject: `Hi, Forgot Your Password?`,
      link: process.env.LOGIN_PAGE,
    },
  })
}

module.exports.sendFinished = async (user, project) => {
  await this.send({
    user: user,
    templateId: process.env.PROJECT_FINISHED_TEMPLATE_ID,
    data: {
      subject: `Congrulations, Your Project is Finished`,
      link: process.env.LOGIN_PAGE,
      project: project,
    },
  })
}

module.exports.send = async (opts) => {
  let { data, user, templateId } = opts

  try {
    await sgmail.send({
      from: `Uxdev <${process.env.OFFICIAL_MAIL}>`,
      to: user.email,
      templateId: templateId,
      dynamicTemplateData: { ...data, user: user },
    })
  } catch (err) {
    throw new ErrorResponse(
      `Couldn't send the email to the adress ${user.email || ''}`,
      500
    )
  }
}
