const Project = require('../models/Project')
const UserService = require('./UserService')
const EmailService = require('./EmailService')

const ErrorResponse = require('../utils/ErrorResponse')
const queryBuilder = require('../helpers/query-builder')
const User = require('../models/User')

module.exports.create = async (props, userId) => {
  if (!userId) {
    let { title, description, wantedBefore, email, name } = props

    let password = UserService.generatePassword(12)

    let user = await UserService.create({
      name: name,
      email: email,
      password: password,
    })

    let project = new Project({
      title,
      description,
      wantedBefore,
      owner: user._id,
    })
    await project.save()
    return project
  }

  let { title, description, wantedBefore } = props

  let user = await UserService.get(userId)

  let project = new Project({
    title,
    description,
    wantedBefore,
    owner: user._id,
  })
  await project.save()

  return project
}

module.exports.confirm = async (id) => {
  // Reset the password of the user
  let project = await Project.findById(id)

  let user = await User.findById(project.owner)

  await project.update({ confirmed: true })

  if (!user.confirmed) {
    let newpass = UserService.generatePassword(12)

    await UserService.resetPassword(user, newpass)

    await EmailService.sendUserInit(user, newpass, project)

    await user.update({ confirmed: true })

    return project
  }
  await EmailService.sendConfirmed(user, project)

  return project
}

module.exports.reject = async (id, message) => {
  let project = await Project.findById(id)
  let user = await User.findById(project.owner)

  await EmailService.sendRejected(user.name, user.email, message)
  await project.remove()
  if (!confirmed) {
    await user.remove()
  }
  return project
}

module.exports.stats = async () => {
  // query for finished projects
  let finisheds = await Project.find({ finished: true }).select('price')

  // total amount of price from finished projects
  let moneyEarned = finisheds
    .map((i) => i.price || 0)
    .reduce((a, b) => a + b, 0)

  // new requests
  let newRequests = await Project.countDocuments({ confirmed: false })

  //customer count
  let customerCount = await User.countDocuments({ role: 'user' })
  return {
    finishedCount: finisheds.length,
    moneyEarned,
    newRequests,
    customerCount,
  }
}

module.exports.update = async (id, props) => {
  let project = await Project.findByIdAndUpdate({ _id: id }, props)
  if (!project) {
    throw new ErrorResponse(`Project with id ${id} not found and updated`, 404)
  }
  return project
}

module.exports.delete = async (id) => {
  let project = await Project.findById(id)
  await project.remove()
  if (!project) {
    throw new ErrorResponse(`Project with id ${id} not found and deleted`, 404)
  }

  return project
}

module.exports.get = async (id) => {
  let project = await Project.findById(id)

  if (!project) {
    throw new ErrorResponse(`Project with id ${id} not found`, 404)
  }
  return project
}

module.exports.query = async (queryParams) => {
  let results = await queryBuilder(queryParams, Project, {
    path: 'owner',
    select: 'name email job company avatar',
  })
  return results
}

module.exports.search = async (keyword) => {}
