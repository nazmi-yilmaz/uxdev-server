const express = require('express')

const router = express.Router()

const authController = require('../controllers/auth')

let { login, forgotPassword } = authController

router.post('/login', login)

router.post('/forgot-password', forgotPassword)

module.exports = router
