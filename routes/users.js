const express = require('express')

const router = express.Router()

const userController = require('../controllers/users')

let { protect, authorize } = require('../middlewares/auth')

let { create, get, query, update, del, resetPassword } = userController

router.put('/reset-password', resetPassword)

router.get('/', protect, authorize('admin'), query)
router.post('/', protect, authorize('admin'), create)

router.get('/:id', protect, get)
router.put('/:id', protect, authorize('admin'), update)
router.delete('/:id', protect, authorize('admin'), del)

module.exports = router
