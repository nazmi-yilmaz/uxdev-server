const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 128,
  },
  description: {
    type: String,
    required: true,
    minlength: 16,
    maxlength: 1000,
  },
  wantedBefore: {
    type: Date,
    required: true,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  stages: [
    {
      done: {
        type: Boolean,
        required: false,
        default: false,
      },
      description: String,
      title: String,
    },
  ],
  channels: [
    {
      name: String,
      link: String,
      detail: String,
    },
  ],
  resource: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Resource',
    required: false,
  },
  demo: {
    type: String,
    required: false,
  },
  madeWith: [
    {
      name: String,
      iconUri: String,
      detail: String,
    },
  ],
  confirmed: {
    type: Boolean,
    required: false,
    default: false,
  },
  finished: {
    type: Boolean,
    required: false,
    default: false,
  },
  price: {
    type: Number,
    required: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
})

schema.pre('remove', async function (next) {
  await this.model('Resource').deleteMany({ files: this._id })
  next()
})

module.exports = mongoose.model('Project', schema)
