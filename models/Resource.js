const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  repository: {
    gitlab: {},
    github: {},
  },
  drive: {
    yandex: {},
    google: {},
    onedrive: {},
  },
})

module.exports = mongoose.model('Resource', schema)
